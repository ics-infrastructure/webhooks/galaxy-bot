import json
import os

f = open(os.environ['TRIGGER_PAYLOAD'])

data = json.load(f)
print(json.dumps(data, indent=2))

f.close()
